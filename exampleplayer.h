#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <map>
#include <string.h>
#include <algorithm>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {
    
private:
    Board myBoard;
    Side mySide;

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    int AlphaBeta(Board *board, int alpha, int beta, Side currSide, int depth);
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif

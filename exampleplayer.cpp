#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {     
     // Create a new board object
     myBoard = Board();
     mySide = side;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {

}


int ExamplePlayer::AlphaBeta(Board *board, int alpha, int beta,
                             Side currSide, int depth) {
    Board *tmpBoard;
    Move *move;
    int i, j, score;
    
    Side otherSide = (currSide == BLACK) ? WHITE : BLACK;
        
    if (depth <= 0) {
        return board->score(mySide);
    }
    
    // Terminal node
    if (!board->hasMoves(currSide)) {
        if (!board->hasMoves(otherSide))
            return board->score(mySide);
        return AlphaBeta(board, alpha, beta, otherSide, depth);    
    }
    
    
    for (i = 0; i < 8; i++) {
       for (j = 0; j < 8; j++) {
            move = new Move(i, j);
            if (board->checkMove(move, currSide)) {
                // Calculate the score after doing this move
                tmpBoard = board->copy();
                tmpBoard->doMove(move, currSide);
                score =  AlphaBeta(tmpBoard, alpha, beta, otherSide, depth - 1);
                // Maximizer
                if (currSide == mySide) {
                    if (score > alpha) {
                        alpha = score;
                    }
                    if (alpha > beta) {
                        return alpha;
                    }
                }
                // Minimizer
                else {
                    if (score < beta) {
                        beta = score;
                    }
                    if (alpha > beta) {
                        return beta;
                    }  
                }
            }
        }
    }	
    
    return (currSide == mySide) ? alpha : beta;
    
}

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {

    // Calculate the opponent's side
    Side oppSide = (mySide == BLACK) ? WHITE : BLACK;

    // Process the opponent's past move.
    myBoard.doMove(opponentsMove, oppSide);
    
    // Check whether there's available moves.
    if (!myBoard.hasMoves(mySide))
        return NULL;
    
    
    Move *move, *bestMove;
    Board *tmpBoard;
    int i, j, score;
    int max_score = -10000;
    string board_rep;

    
    for (i = 0; i < 8; i++) {
       for (j = 0; j < 8; j++) {
            move = new Move(i, j);
            if (myBoard.checkMove(move, mySide)) {
                // Calculate the score after doing this move
                tmpBoard = myBoard.copy();
                tmpBoard->doMove(move, mySide);
                score = AlphaBeta(tmpBoard, -10000, 10000, oppSide, 4);
                if (score > max_score) {
                    max_score = score;
                    bestMove = move;
                }
            }
        }
    }
    
    myBoard.doMove(bestMove, mySide);
    return new Move(bestMove->getX(),bestMove->getY());

}
    

Write-up 
1. Group member contribution

1. Week1 contribution:
Kexin implemented the helper function board.score() and board. stone_weight  of the board class and write a basic player function for our own AI player which beat the SimplePlayer.

Cong wrote a basic two level mini/max algorithm for the AI player and optimize the weight. It beat the constant-time player.

2. Week2 contribution:
Kexin and Cong implemented the Alpha-beta algorithm for the AI together and optimized the heuristics of our AI together. It succeeded in beating the BetterPlayer.


2. Improvements

1. Tested different combinations of stone weights and selected the one that worked the best so far.
2. Tested different search depths for our alpha-beta algorithm, and picked the best one that can finish the game in 16 minutes.
3. Tried to use a hash table as transposition table to store the score of alpha-beta search for given configuration of black and white stones. We are expecting to decrease the  number of searches by looking up the table. However, there is no computer to work on at this time in annerberg. Therefore this implementation was aborted.



#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Helper function that calculates the score of the current board.
 */
int Board::score(Side side) {
    int score = 0;
    Side other = (side == BLACK) ? WHITE : BLACK;
    
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (get(side, i, j)) {
                score += stone_weight(i, j);
            }
            else if (get(other, i, j)) {
                score -= stone_weight(i, j);
            }
        }
    }
        
    return score;
    
}


string Board::toString() {
    string str;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (get(BLACK, i, j))
                str.append("1");
            else if (get(WHITE, i, j))
                str.append("-1");
            else
                str.append("0");
        }
    }
    
    return str;
}

/*
 * Helper function that calculates the score of a stone
 * on given position. Stones are weighted differently
 * according to the amount of advantage it gives us.
 */
int stone_weight(int i, int j) {
    int weight;
    
    switch (i) {
        case 0:
            switch (j) {
                case 0:
                    weight = 100;
                    break;
                case 1:
                    weight = -8;
                    break;
                case 6:
                    weight = -8;
                    break;
                case 7:
                    weight = 100;
                    break;
                default:
                    weight = 8;
                    break;
            }
            break;
            
        case 1:
            switch (j) {
                case 0:
                    weight = -8;
                    break;
                case 1:
                    weight = -30;
                    break;
                case 6:
                    weight = -30;
                    break;
                case 7:
                    weight = -8;
                    break;
                default:
                    weight = 1;
                    break;
            }
            break;
            
        case 6:
            switch (j) {
                case 0:
                    weight = -8;
                    break;
                case 1:
                    weight = -30;
                    break;
                case 6:
                    weight = -30;
                    break;
                case 7:
                    weight = -8;
                    break;
                default:
                    weight = 1;
                    break;
            }
            break;
            
        case 7:
            switch (j) {
                case 0:
                    weight = 100;
                    break;
                case 1:
                    weight = -8;
                    break;
                case 6:
                    weight = -8;
                    break;
                case 7:
                    weight = 100;
                    break;
                default:
                    weight = 8;
                    break;
            }
            break;
            
        default:
            weight = 1;
            break;
    }
    
    return weight;
    
}


